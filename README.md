# README #

Welcome to a demonstration of small multiple structure that includes simple demo of joined horizontal bars to be compared on same x and y axis.

### What is this repository for? ###

* Exercise purposes. To be able to visualize data in multiple dimensions with a common ground by using customized D3.js components.
* Version 1.0

### How do I get set up? ###

* Summary of set up to be run locally

    Install all dependencies from ```package.json```

        npm install

    Run on local server [http://localhost:1234/](http://localhost:1234/)

        npm run develop

        npm run build


* Configuration
* Dependencies

    ```Node.js```, ```npm```

* How to run tests (TODO)
* Deployment instructions (TODO)

### Who do I talk to? ###

Angelina Temelkovska, @AngelinaT
