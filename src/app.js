import * as d3 from "d3";
const csvFile = require("url:./data/Multiples.csv");

let data = null;

async function readData() {
  if (data) return data;

  try {
    data = await d3.csv(csvFile, (d) => ({
      sales: parseInt(d["This Year Sales"].substr(1)),
      district: d["District"],
      category: d["Category"]
    }));
    return data;
  } catch (err) {
    console.error("Error while reading data file: ", err);
  }
}

function groupDataByOne(array, property) {
  return d3.group(array, a => a[property]);
}

function groupDataByTwo(array, property1, property2) {
  return d3.group(array, a => a[property1], a => a[property2]);
}

function sortDataByCategory(array) {
  return d3.groupSort(array, a => d3.ascending(a, b => b.category), b => b.category);
}

function sortDataByLargestSum(array) {
  return d3.groupSort(array, a => -d3.sum(a, b => b.sales), b => b.category);
}

function drawSmallMultiple(xAxisValues, yAxisValues, salesOverallScale, groupedByTwo) {

  const maxWidth = window.innerWidth
    ?? document.documentElement.clientWidth
    ?? document.body.clientWidth - 100;

  const maxHeight = window.innerHeight
    ?? document.documentElement.clientHeight
    ?? document.body.clientHeight - 100;

  const paddingX = 50;
  const paddingY = 50;
  const chartWidth = maxWidth - paddingX;
  const chartHeight = maxHeight * 0.75 - paddingY;
  const nTicksX = Array.from(xAxisValues.keys()).length;

  let svg = d3.select("#SVGContainer")
    .append('svg')
    .attr("width", chartWidth)
    .attr("height", chartHeight);

  //Axes
  const xScale = d3.scalePoint()
    .domain(xAxisValues.keys())
    .rangeRound([paddingX, chartWidth]);
  const yScale = d3.scaleBand()
    .domain(yAxisValues)
    .rangeRound([paddingY, chartHeight])
    .paddingInner(0.4)
    .paddingOuter(0.2);
  const salesScale = d3.scaleLinear()
    .domain(d3.extent(salesOverallScale.keys()))
    .rangeRound([0, maxWidth / nTicksX]);

  svg.append("g")
    .attr("id", "x-axis")
    .attr("transform", `translate(${paddingX + paddingY}, ${paddingY})`)
    .call(d3.axisTop(xScale).ticks(nTicksX).tickSize(-chartHeight))
    .call(g => g.select(".domain").remove());

  svg.append("g")
    .attr("id", "y-axis")
    .attr("transform", `translate(${paddingX / 2}, 0)`)
    .call(d3.axisLeft(yScale).tickSize(0))
    .call(g => g.select(".domain").remove());

  //Main chart bars  
  svg.append("g")
    .attr("id", "chart")
    .attr("transform", `translate(${paddingX + paddingY}, 0)`);

  groupedByTwo.forEach((value, key) => {
    svg.select("#chart")
      .append("g")
      .attr("class", "g-by-vertical")
      .selectAll("rect")
      .data(value.entries())
      .enter()
      .append("rect")
      .attr("x", xScale(key))
      .attr("y", d => yScale(d[0]))
      .attr("height", yScale.bandwidth())
      .attr("width", d => salesScale(d[1][0].sales));
  });
}

async function onInit() {
  const dataRows = await readData();

  const groupedDataByDistrict = groupDataByOne(dataRows, "district");
  const groupedDataByCategory = groupDataByOne(dataRows, "category");
  const groupedDataBySales = groupDataByOne(dataRows, "sales");
  const groupedDataByDistrictAndCategory = groupDataByTwo(dataRows, "district", "category");

  // Unsorted example:
  drawSmallMultiple(groupedDataByDistrict, groupedDataByCategory.keys(), groupedDataBySales, groupedDataByDistrictAndCategory);
}

//Additional feature: Buttons
async function unsort() {
  document.getElementById("SVGContainer").innerHTML = "";
  onInit();
}

async function sortBySum() {
  const dataRows = await readData();
  document.getElementById("SVGContainer").innerHTML = "";

  const groupedDataByDistrict = groupDataByOne(dataRows, "district");
  const groupedDataBySales = groupDataByOne(dataRows, "sales");
  const groupedDataByDistrictAndCategory = groupDataByTwo(dataRows, "district", "category");

  const sortedLargestSumOverall = sortDataByLargestSum(dataRows);

  drawSmallMultiple(groupedDataByDistrict, sortedLargestSumOverall, groupedDataBySales, groupedDataByDistrictAndCategory);
}

async function sortByCategory() {
  const dataRows = await readData();
  document.getElementById("SVGContainer").innerHTML = "";

  const groupedDataByDistrict = groupDataByOne(dataRows, "district");
  const groupedDataBySales = groupDataByOne(dataRows, "sales");
  const groupedDataByDistrictAndCategory = groupDataByTwo(dataRows, "district", "category");

  const sortedByCategory = sortDataByCategory(dataRows);

  drawSmallMultiple(groupedDataByDistrict, sortedByCategory, groupedDataBySales, groupedDataByDistrictAndCategory);
}

document.getElementById("buttonUnsort").addEventListener("click", unsort, false);
document.getElementById("buttonSortBySum").addEventListener("click", sortBySum, false);
document.getElementById("buttonSortByCategory").addEventListener("click", sortByCategory, false);

onInit();